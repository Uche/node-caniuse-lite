# Installation
> `npm install --save @types/caniuse-lite`

# Summary
This package contains type definitions for caniuse-lite (https://github.com/ben-eb/caniuse-lite#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/caniuse-lite.

### Additional Details
 * Last updated: Tue, 17 Oct 2023 22:10:13 GMT
 * Dependencies: none

# Credits
These definitions were written by [Michael Utech](https://github.com/mutech).
